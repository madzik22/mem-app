import React from "react";
import "./Navigation.css";
import { Link } from "react-router-dom";

const Nav = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark memNav">
      <ul className="navbar-nav">
        <li className="nav-item">
          <Link className="nav-link" to="/regular">
            <img
              src={process.env.PUBLIC_URL + "/regular.svg"}
              alt=""
              className="d-inline-block navImg"
            ></img>
            Regular
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/hot">
            <img src={process.env.PUBLIC_URL + "/hot.svg"} alt="" className="d-inline-block navImg"></img>
            Hot
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/addNew">
            Add new
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default Nav;
