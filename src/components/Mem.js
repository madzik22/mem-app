import React from "react";

const Mem = ({ item, updateMem }) => {
  function voteUp() {
    updateMem({ ...item, upvotes: item.upvotes + 1 });
  }
  function voteDown() {
    updateMem({ ...item, downvotes: item.downvotes + 1 });
  }

  return (
    <div className="row justify-content-center">
      <div className="col-6 card">
        <h3>{item.title}</h3>
        <img src={item.img} className="img-fluid" alt=""></img>
        <button type="button" className="btn btn-dark" onClick={voteUp}>
          Like it! {item.upvotes}
        </button>
        <button type="button" className="btn btn-dark" onClick={voteDown}>
          No, no, no! {item.downvotes}
        </button>
      </div>
    </div>
  );
};

export default Mem;
