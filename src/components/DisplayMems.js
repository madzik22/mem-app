import React from "react";
import { useParams } from "react-router";
import Mem from "./Mem";

const DisplayMems = ({mems, updateMem}) => {
  const hotLine = 5;
  const { memtype } = useParams();

  function getMems(type) {
    if (type === "hot") {
      return mems.filter((mem) => mem.upvotes - mem.downvotes >= hotLine);
    }
    if (type === "regular") {
      return mems.filter((mem) => mem.upvotes - mem.downvotes < hotLine);
    }
    return mems;
  }

  return getMems(memtype)
    .map((mem) => (
      <Mem key={mem.id} item={mem} updateMem={updateMem} />
    ));
};

export default DisplayMems;
