import "bootstrap/dist/css/bootstrap.css";
import { useState } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import DisplayMems from "./components/DisplayMems";
import MemForm from "./components/MemForm";
import Nav from "./components/Navigation";

function App() {
  const [state, setState] = useState({
    nextId: 4,
    mems: [
      {
        id: 1,
        title: "Mem 1",
        img: process.env.PUBLIC_URL + "/mem1.png",
        upvotes: 7,
        downvotes: 3,
      },
      {
        id: 2,
        title: "Mem 2",
        img: process.env.PUBLIC_URL + "/mem2.png",
        upvotes: 3,
        downvotes: 3,
      },
      {
        id: 3,
        title: "Mem 3",
        img: process.env.PUBLIC_URL + "/mem3.png",
        upvotes: 3,
        downvotes: 3,
      },
      {
        id: 4,
        title: "Mem 4",
        img: process.env.PUBLIC_URL + "/mem4.png",
        upvotes: 3,
        downvotes: 3,
      },
    ],
  });

  function addMem(mem) {
    mem.id = state.nextId;
    mem.upvotes = 0;
    mem.downvotes = 0;
    setState({
      ...state,
      mems: [...state.mems, mem],
      nextId: state.nextId + 1,
    });
  }

  function updateMem(updatedMem) {
    const newMems = state.mems.map((mem) => {
      if (mem.id === updatedMem.id) {
        return updatedMem;
      }
      return mem;
    });
    setState({ ...state, mems: newMems });
  }

  return (
    <BrowserRouter>
      <div className="App">
        <Nav />
        <Switch>
          <Route path="/addNew">
            <MemForm addMem={addMem} />
          </Route>
          <Route path="/:memtype">
            <DisplayMems mems={state.mems} updateMem={updateMem} />
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
