import { useState } from "react";
import { useHistory } from "react-router-dom";

const MemForm = ({ addMem }) => {
  const [title, setTitle] = useState("");
  const [img, setImg] = useState("");
  const navHistory = useHistory();


  function handleSubmit(event) {
    event.preventDefault();
    addMem({ title, img });
    navHistory.push('/regular');
  }

  return (
    <form className="mt-3 col-md-12 col-lg-6 col-xl-4" onSubmit={handleSubmit}>
      <div className="mb-3 row">
        <label htmlFor="title" className="form-label col-sm-12 col-md-4">
          Mem title
        </label>
        <div className="col-sm-12 col-md-8">
          <input
            id="title"
            name="title"
            type="text"
            className="form-control"
            value={title}
            onChange={(event) => setTitle(event.target.value)}
          />
        </div>
      </div>
      <div className="mb-3 row">
        <label htmlFor="img" className="form-label col-sm-12 col-md-4">
          Image url
        </label>
        <div className="col-sm-12 col-md-8">
          <input
            id="img"
            name="img"
            type="url"
            className="form-control col-sm-12 col-md-8"
            value={img}
            onChange={(event) => setImg(event.target.value)}
          />
        </div>
      </div>
      <button type="submit" className="btn btn-primary">
        Add
      </button>
    </form>
  );
};

export default MemForm;
