The mem application presents a list of memes in the Regular tab. It has a functionality that allows you to vote for memes and add memes to the list from the url address.
Application use react, route, bootstrap 5.

## Run application

In the project directory, you can run:

### `npm install`


### `npm start`

Runs the app in the development mode on port 3000 with json server on port 3004<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
